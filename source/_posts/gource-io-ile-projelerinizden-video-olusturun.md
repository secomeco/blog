---
title: Gource.io ile projelerinizden video oluşturun
date: 2018-04-13 18:24:41
tags: ["gource.io", "git"]
categories:
    - Genel
---
<a href="http://gource.io/" target="_blank" rel="nofollow" title="Gource.io">Gource.io</a>, Git Repository'nizdeki loglar ile animasyonlu bir video oluşturmanızı sağlar. Ayrıca Mercurial, Bazaar ve SVN desteği bulunuyor.

Örnek olarak bir projemde kullandığım video;
<iframe width="560" height="315" style="max-width:100%;" src="https://www.youtube.com/embed/5iMw3Rq9VOg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>