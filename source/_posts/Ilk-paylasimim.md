---
title: İlk paylaşımım
date: 2018-03-04 13:53:23
tags: ['ilk', 'paylasim']
categories:
  - Genel
---
Merhaba, benim adım Sercan. Genellikle yazılım ile ilgileniyorum ve bu sektörde çalışıyorum, bu blogda da çoğunlukla yazılım ile ilgili paylaşımlar yapmayı düşünüyorum. Örneğin; herhangi bir yazılım dilinde karşılaştığım sorunların çözümü, faydalı gördüğüm kütüphaneler veya kendi yapmış olduğum ufak tefek şeyler :) Belki yazılım dışında da hoşuma giden bir şeyler paylaşabilirim :)
